/* MIT License
 * 
 * Copyright (c) 2017 Andy Jiang, Pierre de La Morinerie
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

const TWITTER_API_URL    = 'https://api.twitter.com/';
const TWITTER_UPLOAD_URL = 'https://upload.twitter.com/';

// Manage the authentication flow and authenticated requests
// against the Twitter API.
//
// Usage:
//   let client = new TwitterClient(<your app consumer key>, <your app secret>);
//   let username = await client.loadCredentials();
//   if (!username) {
//     username = await client.authenticate();
//   }
//   await client.sendTweet('Tweet!');
class TwitterClient {
  constructor(consumerKey, consumerSecret) {
    this.consumer_key    = consumerKey;
    this.consumer_secret = consumerSecret;
    this.credentials = new TwitterCredentials();

    this.resolveAuthentication = null;
    this.rejectAuthentication = null;
  }

  // Initialize the client by loading saved Twitter credentials.
  // Returns a promise that will be resolved with the Twitter username if credentials are found,
  // and `null` otherwise.
  async loadCredentials() {
    let credentials = await TwitterCredentials.load();
    if (credentials) {
      this.credentials = credentials;
      return credentials.screen_name;

    } else {
      return null;
    }
  }

  // Start the OAuth flow, and return a promise that will resolve at the end of the flow.
  // 
  // Note: this method will open a pop-up window. To avoid it being blocked by pop-up blockers,
  // call it in the same event loop than the `click` event.
  async authenticate() {
    // Open an empty pop-up window as soon as possible (to avoid pop-up blockers)
    let popup = window.open('about:blank', 'Authenticating with Twitter…');
    if (!popup) {
      throw new Error('[TwitterClient] The pop-up window was blocked. Try disabling the pop-up blocker of your browser.');
    }

    try {
      // Clear existing credentials
      await this._clearCredentials();

      // Retrieve a request token
      let response = await this.api('oauth/request_token', 'POST'),
          responseText = await response.text(),
          responseParams = this._extractParameters(responseText),
          credentials = new TwitterCredentials(responseParams);

      // Load the authentication URL into the pop-up window to start the OAuth flow
      let url = 'https://api.twitter.com/oauth/authenticate?oauth_token=' + credentials.oauth_token;
      popup.location = url;

    } catch (error) {
      if (popup) {
        popup.close();
      }
      throw new Error('[TwitterClient] Error while starting the OAuth flow: ' + error);
    }

    // Listen for messages sent by the background page
    chrome.runtime.onMessage.addListener(this._didReceiveMessage.bind(this));

    // Return a promise that will be resolved (or rejected) when the callback URL is called
    let oauthPromise = new Promise((resolve, reject) => {
      this.resolveAuthentication = resolve;
      this.rejectAuthentication = reject;
    });
    return oauthPromise;
  }

  // Finish the last authentication step, by providing the query parameters
  // sent by Twitter to the callback pop-up page.
  //
  // This method is intended to be called from the background extension page.
  // It will send a message to the content page, in order to resolves (or reject)
  // the promise returned by `authenticate`.
  async completeAuthentication(queryParams) {
    var params = this._extractParameters(queryParams);

    try {
      let response = await this.api('oauth/access_token', 'POST', params),
          responseText = await response.text(),
          responseParams = this._extractParameters(responseText);

      this.credentials = new TwitterCredentials(responseParams);
      await this.credentials.save();
      this._sendMessageToContentPage({ authState: 'success' });
      
    } catch(error) {
      this._sendMessageToContentPage({ authState: 'failure', error: error });
    }
  }

  // Send a request to the Twitter API.
  // Returns a promise that resolves when the request finishes.
  async api(path, method = 'GET', params = {}) {
    // Figure out API host
    let baseUrl = path.match(/upload/) ? TWITTER_UPLOAD_URL : TWITTER_API_URL;
    // Adjust API path (if this is not an authentication request)
    if (!path.match(/oauth/)) {
      path = '1.1/' + path + '.json';
    }

    // Feed the parameters into the OAuth helper
    let url = baseUrl + path;
    let message = {
      action:     url,
      method:     method,
      parameters: params
    };
    OAuth.completeRequest(message, this._defaultOAuthCredentials(this.credentials));

    // Retrieve properly formatted parameters suitable for inclusion in the HTTP request
    // from the OAuth helper
    let requestParams = new URLSearchParams();
    Object.entries(OAuth.getParameterMap(message.parameters)).forEach(([key, value]) => {
      if (value == null) {
        value = '';
      }
      requestParams.append(key, value);
    });

    // Send request
    let response = await fetch(url, { method: method, body: requestParams });

    // Handle non 200-range response codes
    if (response.ok) {
      return response;
    } else {
      let responseText = await response.text();
      throw new Error(`[TwitterClient] API request at '${path}' failed (${responseText})`);
    }
  }

  // Send a file upload request to the Twitter API.
  // Returns a promise that resolves when the request finishes.
  async uploadMedia(mediaUrl) {
    // Download the media file from the external URL
    let mediaResponse = await fetch(mediaUrl);
    let mediaType = mediaResponse.headers.get("content-type");
    let mediaBlob = await mediaResponse.blob();

    // Convert the media to base64
    let mediaBase64 = await new Promise((resolve, reject) => {
      let fileReader = new window.FileReader();
      fileReader.readAsDataURL(mediaBlob);
      fileReader.onload = function() {
        let base64Url = fileReader.result;                
        let base64Content = base64Url.split(',')[1];
        resolve(base64Content);
      };
      fileReader.onerror = function(error) { reject(error); };
    });
   
    // Initialize the media upload
    let initResponse = await this.api('media/upload', 'POST', {
      command: 'INIT',
      total_bytes: mediaBlob.size,
      media_type: mediaType
    });
    let initData = await initResponse.json();
    let mediaId = initData['media_id_string'];

    // Send data
    let appendResponse = await this.api('media/upload', 'POST', {
      command: 'APPEND',
      media_id: mediaId,
      media_data: mediaBase64,
      segment_index: 0
    });
    
    // Finalize download
    let finalizeResponse = await this.api('media/upload', 'POST', {
      command: 'FINALIZE',
      media_id: mediaId
    });
    return finalizeResponse;
  }

  // Use the Twitter API to post a new tweet.
  // The text will be truncated to the maximum length if needed.
  // Usage: twitterClient.postTweet(new Tweet('My status'));
  async sendTweet(tweet) {
    let mediaIds = [];
    if (tweet.medias.length > 0) {
      // Upload all medias in parallel
      let uploads = tweet.medias.map(media => this.uploadMedia(media.url));
      let mediaResponses = await Promise.all(uploads);
      let responsesJson = await Promise.all(mediaResponses.map(response => response.json()));
      mediaIds = responsesJson.map(responseJson => responseJson['media_id_string']);
    }

    let response = await this.api('statuses/update', 'POST', {
      status: tweet.truncatedText(),
      media_ids: mediaIds.join(',')
    });
    return response;
  }

  // Clear user credentials.
  // Returns a promise that will resolve then the credentials have been removed from the persistent store
  async logout() {
    await this._clearCredentials();
  }

  /* Private methods */

  // Returns an object containing OAuth credentials,
  // which can be used as the `accessor` argument of `OAuth.completeRequest`.
  // 
  // OAuth credentials can still be overriden at the request level.
  _defaultOAuthCredentials(credentials) {
    return {
      consumerKey:    this.consumer_key,
      consumerSecret: this.consumer_secret,
      token:          credentials.oauth_token,
      tokenSecret:    credentials.oauth_token_secret
    };
  }

  async _clearCredentials() {
    this.credentials = new TwitterCredentials();
    await this.credentials.save();
  }

  // Convert a query string into an key-value object
  _extractParameters(responseText) {
    let result = {};
    responseText.split('&').forEach((param) => {
      let pair = param.split('=');
      result[pair[0]] = pair[1];
    });
    return result;
  }

  _sendMessageToContentPage(messageObject) {
    chrome.tabs.query({}, function(tabs) {
      for (let tab of tabs) {
        console.debug(`Sending message to tab ${tab.id}`);
        chrome.tabs.sendMessage(tab.id, messageObject, function() {});
      }
    });
  }

  async _didReceiveMessage(request, sender, sendResponse) {
    console.debug("Received message: " + JSON.stringify(request));
    
    if (request.authState == 'success') {
      let screen_name;
      try {
        screen_name = await this.loadCredentials();
      } catch (error) {
        this.rejectAuthentication(error);
      }
      this.resolveAuthentication(screen_name);

    } else if (request.authState == 'failure') {
      this.rejectAuthentication(request.error);
    }

    this.resolveAuthentication = null;
    this.rejectAuthentication = null;
  }
}

// Represent a tweet to be posted.
// Uses twitter-text.js (aliased as window.twttr)
class Tweet {
  get MAX_TWEET_LENGTH() { return 280; }

  constructor(text) {
    this.text = text;
    this.externalUrl = null;
    this.medias = [];
    this.twitterText = window.twttr.txt;
  }

  // Set an URL to the full content, which will be added if the tweet needs to be truncated.
  setExternalUrl(url) {
    this.externalUrl = url;
  }

  addMedia(url, type) {
    this.medias.push({ url, type });
  }

  hasPattern(regexp) {
    return !!this.text.match(regexp);
  }

  deletePattern(regexp) {
    this.text = this.text.replace(regexp, '');
  }

  needsTruncation() {
    return this.twitterText.getTweetLength(this.text) > this.MAX_TWEET_LENGTH;
  }

  // Truncate the text to a size fitting in a tweet.
  //
  // When the text is already short enough, the full text is used.
  // But when the text needs to be truncated, an external URL to the full content is appended (if provided).
  truncatedText() {
    let text = this.text;

    if (! this.needsTruncation()) {
      return text;

    } else {
      let suffix = '…' + (this.externalUrl ? ` ${this.externalUrl}` : ''),
          truncatedText = text;
      for (let length = text.length; this.twitterText.getTweetLength(truncatedText) >= this.MAX_TWEET_LENGTH; length--) {
        truncatedText = text.slice(0, length) + suffix;
      }
      return truncatedText;
    }
  }

}

// Represents, save and retrieve Twitter user credentials from local storage.
// For internal use only.
class TwitterCredentials {
  constructor(credentials) {
    credentials = credentials || {};

    this.oauth_token = credentials.oauth_token || null;
    this.oauth_token_secret = credentials.oauth_token_secret || null;
    this.screen_name = credentials.screen_name || null;
  }

  get isComplete() {
    return this.oauth_token && this.oauth_token_secret && this.screen_name;
  }

  static async load() {
    let results = await _toPromise(chrome.storage.local.get)(['oauth_token', 'oauth_token_secret', 'screen_name']);
    let isComplete = results.oauth_token && results.oauth_token_secret && results.screen_name;
    if (isComplete) {
      return new TwitterCredentials(results);
    } else {
      return null;
    }
  }

  async save() {
    await _toPromise(chrome.storage.local.set)({
      oauth_token:        this.oauth_token,
      oauth_token_secret: this.oauth_token_secret,
      screen_name:        this.screen_name
    });
  }
}

// Helper: takes a function that normally uses a callback as the last argument,
// and returns a function which returns a Promise instead.
function _toPromise(fn) {
  return function(...args) {
    return new Promise((resolve, reject) => {
      try {
        fn(...args, function(...res) {
          if (chrome.runtime.lastError) { throw chrome.runtime.lastError; }
          else { resolve(...res); }
        });
      } catch(e) { reject(e); }
    });
  };
}
