const UIState = {
  SIGNED_OUT: 'signed-out',
  AUTHENTICATING: 'authenticating',
  READY: 'ready',
  POSTING: 'posting',
  SUCCESS: 'success',
  FAILURE: 'failure'
};

const UITootPrivacy = {
  PUBLIC:   'public',
  UNLISTED: 'unlisted',
  PRIVATE:  'private',
  DIRECT:   'direct'
};

// A stateless component that manages the extra UI injected into Mastodon compose form.
//
// The component accept a state object, which contains all the
// state needed for rendering.
//
// Usage:
//   let ui = new BirdSiteUI(document.querySelector('.compose-form'), {
//     checkboxChange: toggleCheckbox,
//     send:           sendTweet
//   });
class BirdSiteUI {
  // Initialize the component.
  // 
  // - composeForm: a node of the Mastodon compose form in which the extra UI should be injected
  // - actions: an object containing two functions called when the associated action is sent:
  //   - checkboxChange(checked): sent when the checkbox is checked or unchecked
  //   - textChange(text):        sent when the toot text changes
  //   - privacyChange(privacy):  sent when the toot is marked or unmarked as "Direct"
  //   - send(toot):              sent when the "Toot" button is clicked
  //   - logout():                sent when the Logout control is clicked
  constructor(composeForm, { checkboxChange, textChange, privacyChange, send, logout }) {
    this.composeForm = composeForm;
    this.actions = { checkboxChange, textChange, privacyChange, send, logout };

    let textarea = this.composeForm.querySelector('textarea');
    textarea.addEventListener('input', this._textareaChanged.bind(this));
    textarea.addEventListener('paste', this._textareaChanged.bind(this));

    let privacyButtonIcon = this.composeForm.querySelector('.privacy-dropdown__value-icon i');
    let privacyObserver = new MutationObserver((/* mutations */) => {
      this._privacyChanged();
    });
    privacyObserver.observe(privacyButtonIcon, { attributes: true, attributeFilter: ['class'] });

    let tootButton = this.composeForm.querySelector('.compose-form__publish button');
    tootButton.addEventListener('click', this._tootButtonClicked.bind(this));
  }

  // Create or update the UI in the compose form from the given state.
  render(state) {
    let viewModel = {
      step:           state.uiState,
      username:       state.username,
      buttonSelected: state.checked,
      buttonVisible:  state.crosspostingAllowed,
      buttonEnabled:  false,
      buttonTitle:    chrome.i18n.getMessage("postOnTheBirdSite"),
      statusVisible:  false,
      statusText:     '',
      statusTitle:    '',
    };

    switch (viewModel.step) {
      case UIState.SIGNED_OUT:
        viewModel.buttonEnabled = true;
        viewModel.statusVisible = false;
        break;
      case UIState.AUTHENTICATING:
        viewModel.buttonEnabled = false;
        viewModel.statusVisible = true;
        viewModel.statusText = chrome.i18n.getMessage("authenticating");
        break;
      case UIState.READY:
        viewModel.buttonEnabled = true;
        viewModel.statusVisible = viewModel.buttonVisible && viewModel.buttonSelected;
        viewModel.statusText = chrome.i18n.getMessage('postAs', viewModel.username);
        viewModel.statusTitle = chrome.i18n.getMessage('clickToLogout');
        break;
      case UIState.POSTING:
        viewModel.buttonEnabled = false;
        viewModel.statusVisible = true;
        viewModel.statusText = chrome.i18n.getMessage("posting");
        break;
      case UIState.SUCCESS:
        viewModel.buttonEnabled = true;
        viewModel.statusVisible = true;
        viewModel.statusText = chrome.i18n.getMessage("sent");
        break;
      case UIState.FAILURE:
        viewModel.buttonEnabled = true;
        viewModel.statusVisible = true;
        viewModel.statusText = chrome.i18n.getMessage("anErrorOccured");
        viewModel.statusTitle = this._encodeHTMLEntities(state.errorMessage);
        break;
      default:
        throw new Error(`Unknow state.uiState '${state.uiState}'`);
        break;
    }

    // Render the component button and status label.
    // (Yes, we do render it from scratch on every change. But the HTML is quite small,
    // and the state doesn't change often: performance is fine, and we don't need
    // a virtual DOM for this.)
    let buttonHtml = `
      <button class="birdsite-button
                     ${viewModel.buttonVisible ? '' : 'birdsite-button--hidden'}
                     ${viewModel.buttonSelected ? 'birdsite-button--selected' : ''}"
              title="${viewModel.buttonTitle}"
              aria-label="${viewModel.buttonTitle}">
        <i class="fa fa-twitter" aria-hidden="true">
      </button>`;

    let statusHtml = `
      <div data-username="${viewModel.username}"
           class="birdsite-status
                  birdsite-status--${viewModel.step}
                  ${viewModel.statusVisible ? '' : 'birdsite-status--hidden'}">
        <p class="birdsite-status__label" title="${viewModel.statusTitle}">
          <i class="birdsite-status__icon fa fa-twitter" aria-label="Twitter"></i>
          <span class="birdsite-status__text">
            ${viewModel.statusText}
          </span>
        </p>
      </div>`;

    // Insert the button
    let buttonsDiv = this.composeForm.querySelector('.compose-form__buttons');
    this._removeChildMatchingSelector(buttonsDiv, '.birdsite-button');
    buttonsDiv.insertAdjacentHTML('beforeend', buttonHtml);
    buttonsDiv.querySelector('.birdsite-button').addEventListener('click', this._buttonClicked.bind(this));

    // Insert the status label
    let legacyMastodonUI = this.composeForm.querySelector('.compose-form__publish .character-counter') !== null;
    if (legacyMastodonUI) {
      // UI for Mastodon < 2.1
      // The status label is inserted under the compose form
      let labelContainer = this.composeForm;
      this._removeChildMatchingSelector(labelContainer, '.birdsite-status');
      labelContainer.insertAdjacentHTML('beforeend', statusHtml);
    
    } else {
      // UI for Mastodon >= 2.1
      // The status label is inserted on the left of the Publish button
      let labelContainer = this.composeForm.querySelector('.compose-form__publish');
      this._removeChildMatchingSelector(labelContainer, '.birdsite-status');
      labelContainer.insertAdjacentHTML('afterbegin', statusHtml);
    }

    if (viewModel.step === UIState.READY) {
      this.composeForm.querySelector('.birdsite-status__text').addEventListener('click', this._logoutClicked.bind(this));
    }
  }

  _removeChildMatchingSelector(parent, selector) {
    let element = parent.querySelector(selector);
    if (element) {
      element.parentNode.removeChild(element);
    }
  }

  _isCrossPostingEnabled() {
    let button = this.composeForm.querySelector('.birdsite-button');
    return button ? button.classList.contains('birdsite-button--selected') : false;
  }

  _hasAttachments() {
    return this.composeForm.querySelectorAll('.compose-form__upload').length > 0;
  }

  _currentTootPrivacy() {
    const privacyMap = {
      'fa-globe':      UITootPrivacy.PUBLIC,
      'fa-unlock-alt': UITootPrivacy.UNLISTED,
      'fa-lock':       UITootPrivacy.PRIVATE,
      'fa-envelope':   UITootPrivacy.DIRECT
    };

    let privacyButtonIcon = this.composeForm.querySelector('.privacy-dropdown__value-icon i');
    let currentIcon = Object.keys(privacyMap)
                        .find(icon => privacyButtonIcon.className.includes(icon));
    return privacyMap[currentIcon];
  }

  // Events

  _textareaChanged(event) {
    let textarea = event.target;
    let toot = textarea.value;
    this.actions.textChange(toot);
  }

  _privacyChanged() {
    let tootPrivacy = this._currentTootPrivacy();
    this.actions.privacyChange(tootPrivacy);
  }

  _buttonClicked() {
    let crossPostingEnabled = this._isCrossPostingEnabled();
    this.actions.checkboxChange(!crossPostingEnabled);
  }

  _tootButtonClicked() {
    let crossPostingEnabled = this._isCrossPostingEnabled(),
        tootPrivacy = this._currentTootPrivacy();

    if (crossPostingEnabled) {
      let textarea = this.composeForm.querySelector('textarea');
      let toot = {
        text:           textarea.value,
        privacy:        this._currentTootPrivacy(),
        hasAttachments: this._hasAttachments()
      };
      if (toot.text.length > 0) {
        this.actions.send(toot);
      }
    }
  }

  _logoutClicked(event) {
    let username = this.composeForm.querySelector('.birdsite-status').getAttribute('data-username');
    if (window.confirm(chrome.i18n.getMessage('confirmLogout', username))) {
      this.actions.logout();
    }
    event.preventDefault();
  }

  /** Helpers */

  _encodeHTMLEntities(htmlString) {
    if (!htmlString) { return null; }

    let entities = {
      '&amp;':  '&',
      '&gt;':   '>',
      '&lt;':   '<',
      '&quot;': '"',
      '&apos;': "'"
    };

    let encodedString = htmlString;
    Object.entries(entities).forEach(([entity, character]) => {
      encodedString = encodedString.replace(new RegExp(character, 'g'), entity);
    });
    return encodedString;
  }
}
